# README #

Node.js local web project to have the ability to upload folder structure as is on the Firebase Storage and make their entries in Realtime Database (as it will be easy to generate their download URL) as Storage has no API of its own.

### What is this repository for? ###

* To have the ability to upload multiple files on Firebase Storage
* **Version:** 1.0

### Steps for installing ###

1. Install node.js server from nodejs.org
2. Install [Webstorm](https://www.jetbrains.com/webstorm/download/)
3. You would have to register online for an account first using [this site](https://www.jetbrains.com/student/) 
4. And then add the license number that you get after registering for the account during the Webstorm installation.
5. Download this project
6. Open the project in Webstorm
7. Update the `var config` in **index.html** with the firebase project where you want to upload the folders to.
8. In the `terminal view` of Webstorm, run the following 2 commands one after the other:
> `npm install express`
> `npm install firebase`
9. Click `Run` on the Menu bar.
10. Click `Edit Configurations...`
11. Click on the green `+`sign on the top left corner.
12. Click on `Node.js` in the Add New Configuration pop-up.
13. Rename it as "server" instead of "unnamed" in the right section
14. Add "index.js" as JavaScript file.
15. Click `Apply`
16. You will see "server" and green run button in the top right corner of Webstorm.
17. Clicking on the green run button will start the server.
18. **Open Chrome or Mozilla Firefox Web Browser.**
19. Type in `localhost:3000` in the URL bar and hit enter. You will see the below screen.
![UploadFolderOnFirebaseStartScreen](https://bitbucket.org/repo/8zzMx4x/images/2600637658-UploadFolderOnFirebaseStartScreen.PNG)
20. You will be able to upload a whole folder but please the follow the instructions listed on the Start Screen before uploading.
21. The folder structure will be uploaded in the folder named `files/` on Firebase Storage and it will be stored as follows in the Firebase Database:
![FirebaseDatabaseFolderStructure.PNG](https://bitbucket.org/repo/8zzMx4x/images/1275759102-FirebaseDatabaseFolderStructure.PNG)
22. Code to fetch these files and methods to query into labels data is in [this repo]
(https://bitbucket.org/dharapb/firebasedemo) (in Android App)


### Acknowledgements ###

* https://gist.github.com/mcdonamp/d4f1327b58ad69334ef06327184df790
* https://github.com/firebase/quickstart-js/tree/master/storage