/**
 * Created by Dhara on 4/25/2017.
 */
var express = require('express');

var app = express();
// configure a public directory to host static content
app.use(express.static(__dirname));

var ipaddress = process.env.OPENSHIFT_NODEJS_IP;
var port      = process.env.OPENSHIFT_NODEJS_PORT || 3000;

app.listen(port, ipaddress);
// local link ::: http://localhost:3000/